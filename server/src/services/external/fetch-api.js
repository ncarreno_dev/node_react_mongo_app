const axios = require("axios");

const fetchFromApi = async () => {
  const URL = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
  const res = await axios.get(URL);
  const data = await res.data;
  return data.hits;
};

module.exports = fetchFromApi;
