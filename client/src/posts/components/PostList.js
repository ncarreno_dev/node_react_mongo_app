import React from "react";
import { List, CircularProgress } from "@material-ui/core";
import PropTypes from "prop-types";
import PostItem from "./PostItem";
import Card from "../../shared/components/Card";
import { sortPosts } from "../../utils/sortPosts";

const PostList = (props) => {
  if (!props.items.length && !props.loading) {
    return (
      <div className="center">
        <Card>
          <h2>No posts found.</h2>
        </Card>
      </div>
    );
  } else if (!props.items.length && props.loading) {
    return (
      <div className="center">
        <CircularProgress color="secondary" />
      </div>
    );
  }
  const posts = sortPosts(props.items);
  return (
    <List component="nav">
      {posts.map((item) => {
        const title = item.title || item.story_title;
        if (title) {
          return (
            <PostItem
              key={item.objectID}
              title={title}
              author={item.author}
              date={item.created_at}
              onDelete={() => props.onDelete(item)}
              onClick={() => props.onClick(item)}
            />
          );
        }
        return null;
      })}
    </List>
  );
};

PostList.propTypes = {
  items: PropTypes.array,
  onClick: PropTypes.func,
  onDelete: PropTypes.func,
  loading: PropTypes.bool,
};

PostList.defaultProps = {
  items: [],
  onClick: () => {},
  onDelete: () => {},
  loading: true,
};

export default PostList;
