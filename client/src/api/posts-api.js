import axios from "axios";

export const getPosts = async () => {
  try {
    const url = "http://localhost:8000/api/posts/all";
    const res = await axios.get(url);
    const data = await res.data;
    return data;
  } catch (err) {
    return [];
  }
};
