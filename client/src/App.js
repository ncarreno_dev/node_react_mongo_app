import React, { useEffect, useState, useCallback } from "react";
import { Container } from "@material-ui/core";
import Header from "./shared/components/MainHeader";
import PostList from "./posts/components/PostList";
import { getPosts } from "./api/posts-api";

const App = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const setPosts = useCallback(async () => {
    setLoading(true);
    const posts = await getPosts();
    setItems(posts);
    setLoading(false);
  }, []);

  // useEffect(() => {
  //   const add = () =>
  //     setTimeout(() => {
  //       setPosts();
  //       setLoading(false);
  //     }, 5000);
  //   add();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  useEffect(() => {
    const add = () =>
      setTimeout(() => {
        setPosts();
      }, 2000);
    add();
  }, [setPosts]);

  const handleOnDelete = (item) => {
    setItems((prevItems) =>
      prevItems.filter((post) => post.objectID !== item.objectID)
    );
  };

  const handleOnClick = (item) => {
    window.open(`${item.story_url}`, "_blank");
  };

  return (
    <>
      <Header />
      <Container>
        <PostList
          items={items}
          onDelete={handleOnDelete}
          onClick={handleOnClick}
          loading={loading}
        />
      </Container>
    </>
  );
};

export default App;
