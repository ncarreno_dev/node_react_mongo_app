const mongoose = require("mongoose");
const Post = require("../models/posts/post-model");
const mongoService = require("../services/mongo-add-data-job");
const { expect, assert } = require("chai");

describe("Cron Job Schedule TEST", () => {
  before(() => {
    mongoose
      .createConnection("mongodb://mongo:27017/node-mongo-api", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        const min = new Date().getMinutes();
        const sec = new Date().getSeconds();
        mongoService.addToMongoJob(sec, min).start();
      });
  });

  describe(" job execution", () => {
    it("expect the insertion", (done) => {
      Post.find().then((res) => {
        expect(res).to.be.an("array");
        assert.ok(res.length > 0, "is populated the bd");
      });
      done();
    });
  });
});
