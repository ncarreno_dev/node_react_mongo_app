module.exports = {
  env: {
    es2020: true,
    node: true,
  },
  extends: ["plugin:react/recommended", "eslint:recommended"],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: "module",
  },
  plugins: ["react", "react-hooks"],
  rules: {
    "react-hooks/rules-of-hooks": "warn",
    "react-hooks/exhaustive-deps": "warn",
    quotes: ["error", "double"],
    "object-curly-spacing": ["error", "always", { objectsInObjects: true }],
    "max-len": ["warn", { code: 80 }],
    "arrow-parens": [2, "always"],
    "no-undef": "off",
  },
};
