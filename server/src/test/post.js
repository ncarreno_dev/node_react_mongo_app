const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
const mongoose = require("mongoose");
chai.should();

chai.use(chaiHttp);

describe("Api Test with data", () => {
  before(() => {
    mongoose.createConnection("mongodb://mongo:27017/node-mongo-api", {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  describe("GET /api/posts/all", () => {
    it("It should GET all the posts", (done) => {
      chai
        .request(server)
        .get("/api/posts/all")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
        });
      done();
    });
  });
});
