const express = require("express");
const router = express.Router();
const postsController = require("../controllers/post/post-controller");

router.get("/posts/all", postsController.listAllPosts);

module.exports = router;
