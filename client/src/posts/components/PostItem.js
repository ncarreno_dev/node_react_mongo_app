import React from "react";
import {
  ListItem,
  ListItemText,
  ListItemIcon,
  Divider,
  makeStyles,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import PropTypes from "prop-types";
import { formatedDate } from "../../utils/dateFormat";
import "./PostItem.css";

const useStyle = makeStyles({
  itemTitle: {
    textAlign: "left",
    width: "40%",
    marginRight: 20,
    color: "#333",
    fontSize: "13px",
  },
});

const PostItem = (props) => {
  const classes = useStyle();
  const handleOnClickItem = (item) => {
    props.onClick(item);
  };

  const handleDeleteItem = (item) => {
    props.onDelete(item);
  };
  return (
    <>
      <ListItem button>
        <ListItemText
          className={classes.itemTitle}
          onClick={() => handleOnClickItem(props.item)}
        >
          <span className="title">{props.title} - </span>
          <span className="author">{props.author}</span>
        </ListItemText>
        <ListItemText>{formatedDate(props.date)}</ListItemText>
        <ListItemIcon>
          <DeleteIcon onClick={() => handleDeleteItem(props.item)} />
        </ListItemIcon>
      </ListItem>
      <Divider />
    </>
  );
};

PostItem.propTypes = {
  item: PropTypes.object,
  author: PropTypes.string,
  date: PropTypes.string,
  title: PropTypes.string,
  onClick: PropTypes.func,
  onDelete: PropTypes.func,
};

PostItem.defaultProps = {
  item: {},
  onClick: () => {},
  onDelete: () => {},
  author: "",
  date: "",
  title: "",
};
export default PostItem;
