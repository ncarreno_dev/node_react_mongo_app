import moment from "moment";

export const sortPosts = (posts) => {
  return posts.sort((a, b) => {
    if (moment(a.created_at).isAfter(moment(b.created_at))) {
      return -1;
    }
    if (moment(a.created_at).isBefore(moment(b.created_at))) {
      return 1;
    }
    return 0;
  });
};
