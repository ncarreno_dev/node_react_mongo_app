const Post = require("../../models/posts/post-model");

const listAllPosts = async (req, res, next) => {
  const posts = await Post.find();
  res.json(posts);
};

exports.listAllPosts = listAllPosts;
