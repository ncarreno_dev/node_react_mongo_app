const mongoose = require("mongoose");

const mongoConnect = mongoose.connect("mongodb://mongo:27017/node-mongo-api", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

exports.mongoConnect = mongoConnect;
