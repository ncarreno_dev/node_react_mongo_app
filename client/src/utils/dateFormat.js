import moment from "moment";

export const formatedDate = (date) => {
  const now = moment().format("YYYY-MM-DD");
  let parserDate = moment(date).format("YYYY-MM-DD");
  let isDiffDay = moment(now).diff(parserDate, "day");

  if (moment(parserDate).isSame(now)) {
    parserDate = moment(date).format("h:mm a");
  } else if (isDiffDay === 1) {
    parserDate = "Yesterday";
  } else if (isDiffDay > 1) {
    parserDate = moment(date).format("MMM DD");
  }
  return parserDate;
};
