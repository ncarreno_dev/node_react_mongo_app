import React from "react";
import { AppBar } from "@material-ui/core";
import "./MainHeader.css";
const Header = () => {
  return (
    <AppBar position="static" color="primary">
      <div className={"header-content"}>
        <h1>HN Feeds </h1>
        <h2>We &lt;3 hacker news! </h2>
      </div>
    </AppBar>
  );
};
export default Header;
