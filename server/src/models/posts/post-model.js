const mongoose = require("mongoose");
const { Schema } = mongoose;

const PostSchema = new Schema({
  _id: Schema.Types.ObjectId,
  created_at: { type: String, required: true },
  title: { type: String, default: null },
  url: { type: String, default: null },
  author: { type: String, default: null },
  points: { type: String, default: null },
  story_text: { type: String, default: null },
  comment_text: { type: String, default: null },
  num_comments: { type: Number, default: null },
  story_id: { type: Number },
  story_url: { type: String },
  story_title: { type: String, default: null },
  _tags: [String],
  objectID: { type: String, default: Math.random() * 10000, required: true },
});

module.exports = mongoose.model("Post", PostSchema);
