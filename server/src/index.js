const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoConfig = require("./config/mongo_connection");
const mongoService = require("./services/mongo-add-data-job");
const HttpError = require("./models/http-error");

const app = express();
const PORT = process.env.PORT || 8000;
app.use(bodyParser.json());
app.use(cors());
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

app.use("/api", require("./routes/post-route"));

app.use((req, res, next) => {
  const error = new HttpError("Could not find this route.", 404);
  throw error;
});

mongoConfig.mongoConnect
  .then(() => {
    const server = app.listen(PORT, () => {
      const HOST = server.address().address;
      const SERVER_PORT = server.address().port;
      console.log(`LISTEN ON host: ${HOST}:${SERVER_PORT}`);
    });
    const minute = new Date().getMinutes();
    const sec = new Date().getSeconds();
    mongoService.addToMongoJob(sec + 1, minute).start();
  })
  .catch((err) => {
    console.log("error en conectar a mongo: ", err);
  });

module.exports = app;
