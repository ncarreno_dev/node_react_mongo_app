# **NODE JS - MONGO - REACT JS - ON - DOCKER APPLICATION.**

## DESCRIPTION

- This application is build with node js environment on a docker container.
  The backend is in server folder.
  The frontend is in client folder.
  Both arquitectures are represented in the images below.

#### SERVER ROOT ARQUITECTURE

---

![](https://i.ibb.co/QkYBkwK/Screen-Shot-2020-06-14-at-16-28-12.png)

> project server root arquitecture.

#### REACT CLIENT ROOT ARQUITECTURE

---

![](https://i.ibb.co/whmtw5H/Screen-Shot-2020-06-14-at-18-26-14.png)

> project client react root arquitecture.

### HOW START THE PROJECT ?

---

#### STEPS

- clone this repository
  `$ git clone https://gitlab.com/ncarreno_dev/node_react_mongo_app.git`
- verify your docker version
  `$ docker --version`
  if not installed go here [get docker](https://docs.docker.com/get-docker)
- start docker
- build our project
  `$ docker-compose build`
- execute the last build
  `$ docker-compose up`
- on your browser open [http://localhost:3000/](http://localhost:3000)

### TESTING

- `$ cd server`
- `$ docker-compose up`

### RUN LINTERS

- `$ cd client`
- `$ npm install && npm run lint`
- `$ npm run lint:fix`
