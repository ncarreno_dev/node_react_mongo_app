const mongoose = require("mongoose");
const { CronJob } = require("cron");
const fetchFromApi = require("./external/fetch-api");
const Post = require("../models/posts/post-model");

const insertPosts = async (posts) => {
  posts.map(async (post) => {
    const {
      created_at,
      title,
      url,
      author,
      points,
      story_text,
      comment_text,
      num_comments,
      story_id,
      story_title,
      story_url,
      tags,
      objectID,
    } = post;
    try {
      const exist = await Post.findOne({ objectID: objectID });
      if (!exist) {
        await new Post({
          _id: mongoose.Types.ObjectId(),
          created_at,
          title,
          url,
          author,
          points,
          story_text,
          comment_text,
          num_comments,
          story_id,
          story_title,
          story_url,
          tags,
          objectID,
        }).save();
      }
    } catch (error) {
      console.error(error);
    }
  });
};

const addToMongoJob = (sec, min) =>
  new CronJob(`${sec} ${min} * * * *`, async () => {
    console.log("start job at min:", +min, "second: ", sec);
    const posts = await fetchFromApi();
    await insertPosts(posts);
    console.log("finish job");
  });

exports.addToMongoJob = addToMongoJob;
